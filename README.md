# kaldi template
# kaldi 教學範例

## 環境 使用docker
請先安裝docker / docker-compose
- docker-compose build
- docker-compose up -d

# 如何進入 docker container 的終端機
- docker exec -it [ 容器ID ] /bin/bash 

## 範例：

- scripts/ 資料夾下的所有檔案會掛載再虛擬機的/app 資料夾 （詳細請看docker-compose.yml）

- 進入到 docker container 的終端機：

1. 到kaldi安裝目錄 /opt/kaldi 看到egs 資料夾, 可以看到常見公開語料庫的範例腳本
2. 選擇yesno 複製到 想執行實驗的目錄
3. 打開切換到s5目錄
4. 打開path.sh 修改 kaldi root＝‘xxxx' 為你/妳的安裝目錄
5. - kaldi的各種運算scripts 通常會放在 個範例的 utils/ 和 step/ 目錄下
    - 並且是由 egs/wsj 複製來的
    - 使用時重新建立新的連結即可，不需要複製一份
    - 指令: ln -s [ kaldi 安裝目錄]/egs/wsj/s5/steps
    - 指令: ln -s [ kaldi 安裝目錄]/egs/wsj/s5/utils

6. 執行 run.sh 即可看到各項步驟執行並產生詞錯誤率的實驗結果
