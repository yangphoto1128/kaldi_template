FROM kaldiasr/kaldi:latest

WORKDIR /app

RUN apt-get update && apt-get install vim git
CMD ping localhost